import csv
from importlib import resources

def get_oms_data():
    """
    $ csvcut -n COVID19-web.27-07-2022.csv 
    1: TrialID
    2: Last Refreshed on
    3: Public title
    4: Scientific title
    5: Acronym
    6: Primary sponsor
    7: Date registration
    8: Date registration3
    9: Export date
    10: Source Register
    11: web address
    12: Recruitment Status
    13: other records
    14: Inclusion agemin
    15: Inclusion agemax
    16: Inclusion gender
    17: Date enrollement
    18: Target size
    19: Study type
    20: Study design
    21: Phase
    22: Countries
    23: Contact Firstname
    24: Contact Lastname
    25: Contact Address
    26: Contact Email
    27: Contact Tel
    28: Contact Affiliation
    29: Inclusion Criteria
    30: Exclusion Criteria
    31: Condition
    32: Intervention
    33: Primary outcome
    34: results date posted
    35: results date completed
    36: results url link
    37: Retrospective flag
    38: Bridging flag truefalse
    39: Bridged type
    40: results yes no
    """
    studies = []

    with resources.open_text("import_resources_example.data", "COVID19-web.27-07-2022.csv") as f:
        rows = csv.DictReader(f)

        for row in rows:
            studies.append(
                {
                    "TrialID": row["TrialID"],
                    "Public-title": row["Public title"],
                    "Date-registration": row["Date registration"],
                    "Source-Register": row["Source Register"],
                    "Recruitment-Status": row["Recruitment Status"],
                    "Study type": row["Study type"]
                }
            )

    return studies

