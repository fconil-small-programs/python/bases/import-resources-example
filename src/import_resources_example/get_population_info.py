import csv
from importlib import resources


def extract_population_general_info():
    general_info = {}

    locations = set()

    with resources.open_text("import_resources_example.data", "WPP2022_TotalPopulationBySex.csv") as f:
        rows = csv.DictReader(f)

        for row in rows:
            current_location = row["Location"]
            if current_location not in locations:
                locations.add(current_location)

                general_info[current_location] = {
                    "Time": row["Time"],
                    "PopTotal": row["PopTotal"],
                    "PopDensity": row["PopDensity"]
                }
    
    return general_info
