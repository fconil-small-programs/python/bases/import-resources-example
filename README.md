# Test package for resources import

Inspired by the following tutorial :

- https://realpython.com/python-import/#resource-imports

Official import documentation :

- https://docs.python.org/3/library/importlib.resources.html

Packaging informations :

- https://packaging.python.org/en/latest/tutorials/packaging-projects/
- https://choosealicense.com/

## Build and install the package

```shell
$ flit build
$ pip install dist/import_resources_example-0.0.1-py3-none-any.whl
```

WARNING : The package is installed in the virtual environment, so the data
files must be placed there which is not a very pleasant location.

```shell
$ tree .venv/lib/python3.10/site-packages/import_resources_example
├── data
│   └── __init__.py
├── get_oms_data.py
├── get_population_info.py
└── __init__.py
```

When installing the package in development mode with `pip install -e .`, the
data folder is in the clone repository.

## Usage

Install the package and try to execute the functions.

```ipython
In [1]: from import_resources_example import get_population_info

In [2]: get_population_info.extract_population_general_info()
{'AUKUS': {'Time': '1950', 'PopTotal': '206513.782', 'PopDensity': '12.096'},
 'African Group': {'Time': '1950',
  'PopTotal': '227261.208',
  'PopDensity': '7.761'},
 'African Union': {'Time': '1950',
  'PopTotal': '227274.19',
  'PopDensity': '7.692'},

In [3]: from import_resources_example import get_oms_data

In [4]: get_oms_data.get_oms_data()
Out[4]: 
[{'TrialID': 'NCT00173563',
  'Public-title': 'Induction of Cytokines in Human Monocytes by SARS-CoV in Adults and Children',
  'Date-registration': '12/09/2005',
  'Source-Register': 'ClinicalTrials.gov',
  'Recruitment-Status': 'Recruiting',
  'Study type': 'Observational'},
 {'TrialID': 'NCT00523276',
  'Public-title': 'SARS Survivor Evaluations',
  'Date-registration': '30/08/2007',
  'Source-Register': 'ClinicalTrials.gov',
  'Recruitment-Status': 'Not recruiting',
  'Study type': 'Observational'},
```

## Remark

The data files do not need to be packaged, just place them where the data
folder is installed.

WARNING : you have to manually remove the data files copied in the data folder
installed in `site-packages` with pip if you want to remove the package.

```shell
$ pip uninstall import_resources_example
Found existing installation: import_resources_example 0.0.1
Uninstalling import_resources_example-0.0.1:
  Would remove:
    /home/fconil/Progs/python/imports/resources/.venv/lib/python3.10/site-packages/import_resources_example-0.0.1.dist-info/*
    /home/fconil/Progs/python/imports/resources/.venv/lib/python3.10/site-packages/import_resources_example/*
  Would not remove (might be manually added):
    /home/fconil/Progs/python/imports/resources/.venv/lib/python3.10/site-packages/import_resources_example/data/COVID19-web.27-07-2022.csv
    /home/fconil/Progs/python/imports/resources/.venv/lib/python3.10/site-packages/import_resources_example/data/WPP2022_TotalPopulationBySex.csv
Proceed (Y/n)? y
  Successfully uninstalled import_resources_example-0.0.1
```

## Troubleshooting

If we have untracked data files in the package tree, flist refuses to build the
package.

```shell
$ flit build
Fetching list of valid trove classifiers                                                                                                                                                    I-flit.validate
Untracked or deleted files in the source directory. Commit, undo or ignore these files in your VCS.
```

If we add a line to ignore, e.g. all csv files, in `.gitignore` we get a
warning when we want to add one.

```shell
$ git add WPP2022_TotalPopulationBySex.csv 
Les chemins suivants sont ignorés par un de vos fichiers .gitignore :
src/import_resources_example/data/WPP2022_TotalPopulationBySex.csv
astuce: Utilisez -f si vous voulez vraiment les ajouter.
astuce: Éliminez ce message en lançant
astuce: "git config advice.addIgnoredFile false"
```

As we can append the data files with `-f`, I will ignore the data file in `.gitignore`.

```shell
$ git add -f WPP2022_TotalPopulationBySex.csv 
$
```
